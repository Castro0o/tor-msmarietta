#!/usr/bin/env python3

import os
import subprocess
import shlex
from datetime import datetime

# list all files
project_dir = "/home/andre/Documents/Projects/Tor/"#/home/pi/"  # CHANGE ON PI
audio_dir = project_dir + "tracks/"  # CHANGE ON PI
now = str(datetime.now())

print(os.listdir(audio_dir))
audio_files = [audio_dir + f for f in os.listdir(audio_dir) if ".wav" in f]
print(audio_files)

# what file to play now?
with open(project_dir + 'current_audio.txt', 'r') as current_audio:
    try:
        current_audio_number = int(current_audio.read())
    except: # if cannot find string in current_audio.txt
        current_audio_number = 0
    print('current_audio_number:', current_audio_number)

# what file to play next
with open(project_dir + 'current_audio.txt', 'w') as current_audio:
    next_audio_number = (current_audio_number + 1) % len(audio_files)
    current_audio.write(str(next_audio_number))


# select current audio
current_audio = audio_files[current_audio_number]
print(current_audio)

# log current file
with open(project_dir + 'playback.log', 'w') as logfile:
    logfile.write('{}  \nPLAYING {}'.format(now, current_audio))

# play audio file
aplay_cmd = "aplay -f cd {}".format(current_audio)
aplay_cmd = shlex.split(aplay_cmd)
subprocess.call(aplay_cmd)
