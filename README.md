
# TOR 


Audio files:
- filename: use no spaces
- format: .wav, 16bit, 48KHz

Schedule for playback
- every 2 hours 
- between 11:00 - 20:00
- 5 track: 11, 13, 15, 17, 19
- cron job `0 11-20/2 * * * /home/pi/playaudio.py`


# debug:
* look at playback.log
* check wheter cronjob is pointing to the correct file
* is aplay running while script is playing
* is alsa volume up?

# Pi get time when offline
The Raspberry Pi does not have real time clock

* disable NTP server 
    - `sudo systemctl status systemd-timesyncd.service` ntp running?
    - `sudo systemctl stop systemd-timesyncd.service` stop ntp
    - `sudo systemctl disable systemd-timesyncd.service` prevent it from restarting when machine reboots
    - test: date should not change 
* write systemd to set date on boot to HH:MM:SS
    - /etc/systemd/system/settime.service
  ```
[Unit]
Description=setstarttime
After=sound.target

[Service]
ExecStart=/home/pi/settime.sh

[Install]
WantedBy=default.target

  ```  

    - `sudo date -s '2019-06-22 14:45:00'`  
    - can date be set without password?  
* test cronjob every minute run greeting sh script
    - make it write to time.log 

I reboot
see if logged date corresponds to this moment

check systemd

if so activate cronjob
